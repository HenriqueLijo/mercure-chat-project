<?php

namespace App\Controller;

use App\Entity\Channel;
use App\Repository\ChannelRepository;
use App\Repository\MessageRepository;
use App\Services\Mercure\CookieJwtProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\WebLink\Link;

class ChannelController extends AbstractController
{

    public function getChannels(ChannelRepository $ChannelRepository ): Response
    {
        $channels = $ChannelRepository->findAll();

        return $this->render('channel/index.html.twig', [
            'channels' => $channels ?? []
        ]);
    }

    public function chat(
        Request $request,
        Channel $channel,
        MessageRepository $messageRepository,
        CookieJwtProvider $cookieJwtProvider
    ): Response
    {
        $messages = $messageRepository->findBy([
            'channel' => $channel
        ], ['createdAt' => 'ASC']);

        $hubUrl = $this->getParameter('mercure.default_hub');
        $this->addLink($request, new Link('mercure', $hubUrl));

        $response = $this->render('channel/chat.html.twig', [
            'channel' => $channel,
            'messages' => $messages
        ]);
        $response->headers->setCookie(
            Cookie::create(
                'mercureAuthorization',
                $cookieJwtProvider($channel),
                new \DateTime('+1day'),
                '/.well-known/mercure'
            )
        );
        return $response;
    }

}
